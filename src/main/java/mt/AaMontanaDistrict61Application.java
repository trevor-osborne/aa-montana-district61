package mt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AaMontanaDistrict61Application {

	public static void main(String[] args) {
		SpringApplication.run(AaMontanaDistrict61Application.class, args);
	}

}
