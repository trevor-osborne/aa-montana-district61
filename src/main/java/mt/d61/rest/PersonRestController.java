package mt.d61.rest;

import io.swagger.v3.oas.annotations.Parameters;
import mt.d61.model.pojo.Person;
import mt.d61.model.spi.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.*;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Pageable;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@RestController
public class PersonRestController {

    @Autowired
    PersonRepository personRepository;

/*
    @GetMapping(path="/persons/findBy")
    public Page<Person> findBy(@QuerydslPredicate(root = Person.class) Predicate predicatePerson, Pageable pageablePerson) {
        return personRepository.findAll(predicatePerson, pageablePerson);
    }
*/

    @GetMapping(path = "/persons")
    public List<Person> getAll() {
        return StreamSupport
                .stream(personRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    @GetMapping(path = "/persons/{id}")
    public Person get(@PathVariable String id) {
        return personRepository.findById(Long.parseLong(id)).get();
    }

    @PostMapping(path = "/persons")
    public Person save(@Valid @RequestBody Person person) {
        return personRepository.save(person);
    }

    @PatchMapping(path = "/persons/{id}")
    public Person update(@Valid @RequestBody Person person, @PathVariable String id) {
        return personRepository.save(person);
    }
    // TODO: This updates whole object instead of just values provided in body. Also, might
    //  see how you can submit without including primary key in body and just header.
    //  The annotation I thought that would allow updating only specific fields is over the
    //  pojo. Right now, fields not included are set null. Not a true PATCH.

    @DeleteMapping(path = "/persons")
    public void deleteAll() {
        personRepository.deleteAll();
    }

    @DeleteMapping(path = "/persons/{id}")
    public void delete(@PathVariable Long id) {
        personRepository.deleteById(id);
    }
}
