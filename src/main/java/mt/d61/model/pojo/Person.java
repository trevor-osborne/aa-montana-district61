package mt.d61.model.pojo;

import javax.persistence.*;
import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

/**
 * POJO representing a Person within the data store.
 *
 */
@Entity
@Data
@EqualsAndHashCode
@RequiredArgsConstructor
@ToString
@DynamicUpdate
public class Person implements  Serializable {

    @EqualsAndHashCode.Include
    @Column(name="PERSON_ID")
    @NotNull
    private @Id @GeneratedValue(strategy=GenerationType.AUTO) Long personId;
    @Column(name="FIRST_NAME")
    @NotEmpty
    @Size(max = 30, min = 1)
    private String firstName;
    @Column(name="LAST_NAME")
    @Size(max= 30)
    private String lastName;
    @Column(name="CHAIR_POSITION")
    @Size(max= 30)
    private String chairPosition;
    @Column(name="MOBILE_NUMBER")
    @Size(max= 30)
    private String mobileNumber;
    @Column(name="EMAIL_ADDRESS")
    @Size(max= 30)
    private String emailAddress;
}

