package mt.d61.model.spi;

import mt.d61.model.pojo.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * Spring data repository for {@link Person} objects.
 *
 */
@Repository
public interface PersonRepository extends
        PagingAndSortingRepository<Person, Long>, QuerydslPredicateExecutor<Person> {
}

