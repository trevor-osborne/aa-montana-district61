DROP TABLE IF EXISTS Person;

CREATE TABLE person (
                              PERSON_ID LONG AUTO_INCREMENT  PRIMARY KEY,
                              FIRST_NAME VARCHAR(250) ,
                              LAST_NAME VARCHAR(250) ,
                              CHAIR_POSITION VARCHAR(250) ,
                              MOBILE_NUMBER VARCHAR(250) ,
                              EMAIL_ADDRESS VARCHAR(250)
);

INSERT INTO person (PERSON_ID, FIRST_NAME, LAST_NAME, CHAIR_POSITION, MOBILE_NUMBER, EMAIL_ADDRESS) VALUES
(1, 'Bob', 'Andrews', 'DCM', '406-334-6767', 'bob@aa.org'),
(2, 'Bill', 'Wilson', 'Grape Vine', '406-862-7890', 'bill@aa.org'),
(3, 'Trevor', 'Osborne', 'Treatment', '760-473-6713', 'trevorosborne89@yahoo.com');
commit;