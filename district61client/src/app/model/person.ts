export class Person {
  personId: string;
  firstName: string;
  lastName: string
  chairPosition: string;
  mobileNumber: string;
  emailAddress: string;

}
